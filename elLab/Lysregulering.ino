// Rød LED
int red = 9;
// Gul LED
int yellow = 10;
// Grøn LED
int green = 11;

//Setup
void setup()
{
  // Alle pins er output
  pinMode(red, OUTPUT);
  pinMode(yellow, OUTPUT);
  pinMode(green, OUTPUT);
}

void loop() {
  //Tænd rød, vent 1,5s
  light(red);
  delay(1500);
  //Tænd gul, vent 1s
  light(yellow);
  delay(1000);
  //Sluk rød + gul, tænd grøn, vent 2,5s (samme som rød lyste total)
  darken(red);
  darken(yellow);
  light(green);
  delay(2500);
  //Sluk grøn, tænd gul, vent 1s
  darken(green);
  light(yellow);
  delay(1000);
  // Gul er den sidste der lyser, sluk den
  darken(yellow);
  //Loopet vil starte forfra med det samme (intet delay)
}

// Forkortelse af digitalWrite(pin, HIGH)
void light(int pin) {
  digitalWrite(pin, HIGH);
}

// Forkortelse af digitalWrite(pin, LOW)
void darken(int pin) {
  digitalWrite(pin, LOW);
}

// Tænder en pin, venter den givne tid, og slukker den igen
void blinkLED(int pin, int time) {
 light(pin);
 delay(time);
 darken(pin);
 delay(time);
}

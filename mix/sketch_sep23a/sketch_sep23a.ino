int buzzer = 13;
int flex = A0;
int led = 9;
#define DELAY_OFFSET 11

void setup() {
  pinMode(buzzer, OUTPUT);
  pinMode(led, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  //
  //delay(50);
  Serial.println(analogRead(flex));
  beep(analogRead(flex), 250, buzzer);
  int value = analogRead(flex);
  analogWrite(led, value/4);
}

void on(int pin) {
  digitalWrite(pin, HIGH);
}

void off(int pin) {
  digitalWrite(pin, LOW);
}

void beep(unsigned long hz, unsigned long ms, int BEEP_PIN) {
// reference: 440hz = 2273 usec/cycle, 1136 usec/half-cycle
unsigned long us = (500000 / hz) - DELAY_OFFSET;
unsigned long rep = (ms * 500L) / (us + DELAY_OFFSET);
for (int i = 0; i < rep; i++) {
digitalWrite(BEEP_PIN, HIGH);
delayMicroseconds(us);
digitalWrite(BEEP_PIN, LOW);
delayMicroseconds(us);
}
}


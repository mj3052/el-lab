int count = 13;
int o0 = 2;
int o1 = 3;
int o2 = 4;
int o3 = 5;
int dA = 8;
int dB = 9;
int dC = 10;
int dD = 11;
int reset = 6;
int isReset = 0;
int mic = A0;

void setup() {
  pinMode(count, OUTPUT);
  pinMode(o3, INPUT);
  pinMode(o2, INPUT);
  pinMode(o1, INPUT);
  pinMode(o0, INPUT);
  pinMode(dA, OUTPUT);
  pinMode(dB, OUTPUT);
  pinMode(dC, OUTPUT);
  pinMode(dD, OUTPUT);
  pinMode(reset, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  //mode1();
  //mode2();
  if(isReset == 0) {
    ping(reset);
    isReset = 1;
  }
  
   int micRead = analogRead(mic);
  //Serial.println(micRead);
  if(micRead > 710) {
    Serial.println("LOUD!");
    ping(count);
    writeBit(dA, digitalRead(o0));
    writeBit(dB, digitalRead(o1));
    writeBit(dC, digitalRead(o2));
    writeBit(dD, digitalRead(o3));
    
    delay(100);
  }
}

void ping(int pin) {
  digitalWrite(pin, HIGH);
  delay(10);
  digitalWrite(pin, LOW);
}

void mode1() {
  if(isReset == 0) {
    ping(reset);
    isReset = 1;
  }
  ping(count);
  
  int d1 = digitalRead(o0);
  int d2 = digitalRead(o1);
  int d3 = digitalRead(o2);
  int d4 = digitalRead(o3);
  int sum = d1*1 + d2*2 +d3*4 + d4*8;
  Serial.print(sum);
  Serial.print("\n");
  
  delay(1000);
}

void mode2() {
  if (Serial.available() > 0) {
  
  String response(Serial.read());
  Serial.print(response);
  clean();
  if(response == "48") {
    // 0
    digitalWrite(dA, LOW);
    digitalWrite(dB, LOW);
    digitalWrite(dC, LOW);
    digitalWrite(dD, LOW);
  }
  else if(response == "49") {
    digitalWrite(dA, HIGH);
    digitalWrite(dB, LOW);
    digitalWrite(dC, LOW);
    digitalWrite(dD, LOW);
  }
  else if(response == "50") {
    digitalWrite(dA, LOW);
    digitalWrite(dB, HIGH);
    digitalWrite(dC, LOW);
    digitalWrite(dD, LOW);
  }
  else if(response == "51") {
    digitalWrite(dA, HIGH);
    digitalWrite(dB, HIGH);
    digitalWrite(dC, LOW);
    digitalWrite(dD, LOW);
  }
  else if(response == "52") {
    digitalWrite(dA, LOW);
    digitalWrite(dB, LOW);
    digitalWrite(dC, HIGH);
    digitalWrite(dD, LOW);
  }
  else if(response == "53") {
    digitalWrite(dA, HIGH);
    digitalWrite(dB, LOW);
    digitalWrite(dC, HIGH);
    digitalWrite(dD, LOW);
  }
  else if(response == "54") {
    digitalWrite(dA, LOW);
    digitalWrite(dB, HIGH);
    digitalWrite(dC, HIGH);
    digitalWrite(dD, LOW);
  }
  else if(response == "55") {
    digitalWrite(dA, HIGH);
    digitalWrite(dB, HIGH);
    digitalWrite(dC, HIGH);
    digitalWrite(dD, LOW);
  }
  else if(response == "56") {
    digitalWrite(dA, LOW);
    digitalWrite(dB, LOW);
    digitalWrite(dC, LOW);
    digitalWrite(dD, HIGH);
  }
  else if(response == "57") {
    digitalWrite(dA, HIGH);
    digitalWrite(dB, LOW);
    digitalWrite(dC, LOW);
    digitalWrite(dD, HIGH);
  } else {
    Serial.print("DAMN!");
  }
  
  }
}

void clean() {
  digitalWrite(dA, LOW);
  digitalWrite(dB, LOW);
  digitalWrite(dC, LOW);
  digitalWrite(dD, LOW);
}

void writeBit(int pin, int number) {
  if(number == 1) {
    digitalWrite(pin, HIGH);
  }
  else {
    digitalWrite(pin, LOW);
  }
}

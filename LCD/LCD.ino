#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE); // Addr, En, Rw, Rs, d4, d5, d6, d7, backlighpin, polarity


int meter = A0;
int sound = 13;

void setup()
{
lcd.begin(16,2);
lcd.backlight();
pinMode(sound, OUTPUT);
}

void loop()
{
  lcd.clear();
  String meterRead = String(analogRead(meter));
  lcd.print("Sound: ");
  lcd.print(meterRead);
  lcd.setCursor(0,1);
  lcd.print("");
  
  //beep(analogRead(meter),250,sound);
  delay(100);
}

// Fade lines away on LCD
void wipeLines() {
for (int y = 0; y < 4; y++) {
for (int x = 0; x < 20; x++) {
lcd.setCursor (x,y);
lcd.print(" ");
delay(10);
}
}
}


// Void for buzzer
void beep(unsigned long hz, unsigned long ms, int BEEP_PIN) {
// reference: 440hz = 2273 usec/cycle, 1136 usec/half-cycle
unsigned long us = (500000 / hz) - 11;
unsigned long rep = (ms * 500L) / (us + 11);
for (int i = 0; i < rep; i++) {
digitalWrite(BEEP_PIN, HIGH);
delayMicroseconds(us);
digitalWrite(BEEP_PIN, LOW);
delayMicroseconds(us);
}
}
